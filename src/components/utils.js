import axios from 'axios';

const PROXY = '//cors-anywhere.herokuapp.com/';
const BASE_URL = '//api.sputnik8.com/v1/';
const API_KEY = '4b6ff33993f4851ccadbd79ca50c2b05';
const USERNAME = 'nikita.madin@gmail.com';

export const HTTP = axios.create({
    baseURL: `${PROXY}${BASE_URL}`,
    params: {
        api_key : API_KEY,
        username: USERNAME
    }
})

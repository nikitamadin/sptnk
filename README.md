# sptnk

#### Build Setup

``` bash
# install dependencies
npm i

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build
```

### Demo
(http://sptnk.herokuapp.com/)
